from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from config.configs import color


# classe para manipular o socket
class Send:
    def __init__(self):
        self.__msg = ''
        self.new = True
        self.con = None

    def put(self, msg):
        self.__msg = msg
        if self.con != None:
            # envia um mensagem atravez de uma conexão socket
            self.con.send(str.encode(self.__msg))

    def get(self):
        return self.__msg

    def loop(self):
        return self.new


# função esperar - Thread
def esperar(tcp, send, host='localhost', port=5000):
    destino = (host, port)
    # conecta ao servidor
    tcp.connect(destino)

    while send.loop():
        print(color.YELLOW + 'Conectado a ', host, '.' + color.END)
        # atribui a conexão ao manipulador
        send.con = tcp
        while send.loop():
            # aceita uma mensagem
            msg = tcp.recv(1024)
            if not msg: break
            print(str(msg, 'utf-8'))


if __name__ == '__main__':
    print(color.GREEN + 'Digite o seu nome: ' + color.END)
    ClienteName = input()

    print(color.YELLOW + 'Olá, ' + ClienteName + '. Meu nome é Luna, sou a I.A do Chat, estarei aqui para lhe auxiliar no que for preciso.' + color.END)

    print(color.GREEN + '\nPor favor, digite o nome ou IP do servidor(localhost): ' + color.END)
    host = input()

    if host == '':
        host = '127.0.0.1'

    # cria um socket
    tcp = socket(AF_INET, SOCK_STREAM)
    send = Send()
    # cria um Thread e usa a função esperar com dois argumentos
    processo = Thread(target=esperar, args=(tcp, send, host))
    processo.start()
    print('')

    msg = color.DARKCYAN + input() + color.END 
    while True:
        send.put(color.BOLD + ClienteName + ': ' + msg + color.END + '\n')
        print('-----------------------------------------------------------------------------------')
        print(color.BOLD + "Digite sua mensagem: " + color.END)
        msg = color.DARKCYAN + input() + color.END

    processo.join()
    tcp.close()
    exit()